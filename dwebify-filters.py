#!/usr/bin/env python3


import asyncio
import aiohttp
import aioipfs
import copy
import sys
import tempfile
import yaml
from pathlib import Path
from yarl import URL
from yaml import Loader


async def httpGet(url: str):
    try:
        async with aiohttp.ClientSession() as sess:
            async with sess.get(url) as resp:
                return await resp.read()
    except Exception as err:
        print(f'httpGet({url}) error: {err}', file=sys.stderr)


async def main(src: str, outputp: str):
    rps = 'default'
    pinremote = True
    client = aioipfs.AsyncIPFS()

    if not await client.id():
        print('Unable to connect to IPFS node')
        sys.exit(1)

    try:
        with open(src, 'rt') as fd:
            db = yaml.load(fd, Loader)
    except Exception as e:
        print(f'Cannot load filters descriptor: {e}')
        sys.exit(1)

    add_options = {
        'cid_version': 1,
        'wrap_with_directory': False,
        'recursive': True,
        'quiet': True
    }

    pinned = []

    try:
        async for entry in client.pin.remote.ls(rps, status=['pinned']):
            pinned.append(entry)
    except Exception as err:
        print(f'Could not list remote pins: {err}')

    pinned_cids = [p['Cid'] for p in pinned]

    dwebified = copy.copy(db)

    for provider, provider_fsets in db.items():
        print(f'Processing provider: {provider}')

        for fset_name, fset_descr in list(provider_fsets.items()):
            dsection = copy.copy(fset_descr)

            if 'url' not in fset_descr:
                continue

            url = URL(fset_descr.get('url'))
            data = await httpGet(str(url))

            if not data:
                continue

            with tempfile.TemporaryDirectory() as contdir:
                dst = Path(contdir).joinpath(url.host).joinpath(
                    url.path.lstrip('/'))
                dst.parent.mkdir(parents=True, exist_ok=True)

                with open(str(dst), 'wb') as file:
                    file.write(data)
                    file.close()

                entries = [e async for e in
                           client.add(contdir, **add_options)]
                if not entries:
                    print(f'Could not import: {contdir}')
                    continue

                cid = entries[-1]['Hash']

                print(f'Imported filters from: {url}')

                await asyncio.sleep(3)

                print(f'Pin remote: {cid}')

                try:
                    assert pinremote is True

                    if cid not in pinned_cids:
                        await client.pin.remote.add(
                            rps,
                            cid
                        )
                    else:
                        print(f'{cid}: already pinned to remote')
                except aioipfs.APIError as err:
                    print(f'Could not remote pin: {cid}: {err.message}')
                except Exception as err:
                    print(f'Exception on remote pin: {cid}: {err}')

                fp = url.host + url.path
                path = f'/ipfs/{cid}/{fp}'

                dsection['ipfsPath'] = path

            dwebified[provider][fset_name] = dsection
            del dwebified[provider][fset_name]['url']

    with open(outputp, 'wt') as out:
        yaml.dump(dwebified, out)


if __name__ == '__main__':
    if sys.version_info.major == 3 and sys.version_info.minor < 10:
        loop = asyncio.get_event_loop()
    else:
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError:
            loop = asyncio.new_event_loop()

        asyncio.set_event_loop(loop)

    loop.run_until_complete(main(sys.argv[1], sys.argv[2]))
